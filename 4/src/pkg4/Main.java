package pkg4;

public class Main {

    public static void main(String[] args) {
       
        Customer firstCustomer = new Customer(15, "Guian Amancio ", 25);
        System.out.println(firstCustomer);  // Customer's toString()
        firstCustomer.setDiscount(20);
        System.out.println(firstCustomer);
        System.out.println("id of Customer is:  " + firstCustomer.getID());
        System.out.println("Name of Customer is:  " + firstCustomer.getName());
        System.out.println("Discount is: " + firstCustomer.getDiscount());

        Invoice invoice1 = new Invoice(100, firstCustomer, 250.8);
        System.out.println(invoice1);
        invoice1.setAmount(500);
        System.out.println(invoice1);
        System.out.println("id is: " + invoice1.getID());
        System.out.println("customer is: " + invoice1.getCustomer());
        System.out.println("amount is: " + invoice1.getAmount());
        System.out.println("The customer's id is: " + invoice1.getCustomerID());
        System.out.println("The customer's name is: " + invoice1.getCustomerName());
        System.out.println("The customer's discount is: " + invoice1.getCustomerDiscount());
        System.out.printf("The amount after being discounted is: %.2f%n", invoice1.getAmountAfterDiscount());
    }
}
    