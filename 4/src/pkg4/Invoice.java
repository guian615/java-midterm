
package pkg4;

public class Invoice {
  private int id;
  private  Customer customer;
  private  double amount;

   public Invoice(int id, Customer customer, double amount) { 
       this.id = id;
       this.customer = customer;
       this.amount = amount;
   }
   
   public int getID(){
     return id; 
   }  
   
   public String getCustomer(){
        return customer.toString(); 
   }
   
   public void setCustomer(Customer customer){
       this.customer= customer;
   }
   
   public double getAmount(){
       return amount;  
   }
   
   public void setAmount(double amount){
     this.amount = amount;  
   }
   
   public int getCustomerID(){
     return customer.id;    
   }
   
   public String getCustomerName(){
      return customer.name; 
   }
   
   public int getCustomerDiscount(){
       return customer.discount; 
   }
   
   public double getAmountAfterDiscount(){
     return (customer.discount+100);   
   }  
   
   public String toString(){
       return "Invoice[id="+id+",customer = "+getCustomer()+",amount = "+amount+"]";  
   }
}
    