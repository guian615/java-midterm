package pkg3;

public class Main {

    public static void main(String[] args) {
        MyDate currentdate = new MyDate(2021, 12, 23);

        MyDate date1 = new MyDate(2021, 03, 26);
        System.out.println("Date chosen is " + date1);
        System.out.println("Next day is " + date1.nextDay());
        System.out.println("Next next day is " + date1.nextDay());
        System.out.println("Next month is " + date1.nextMonth());
        System.out.println("Next year is " + date1.nextYear());

        MyDate date2 = new MyDate(2020, 12, 29);
        System.out.println("Date chosen is " + date2);
        System.out.println("Other day is " + date2.previousDay());
        System.out.println("The other day is " + date2.previousDay());
        System.out.println("Previous month is " + date2.previousMonth());
        System.out.println("The previous year is " + date2.previousYear());

    }
}
