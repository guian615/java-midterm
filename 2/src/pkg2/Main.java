package pkg2;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
       
        System.out.println("\nTest for Customers:\n");
        Customer firstCustomer = new Customer(" Guian Amancio ");
        firstCustomer.setMemberType("Premium");
        System.out.println("Customer: " + firstCustomer.getName()); 
        System.out.println("Member Type: " + firstCustomer.getMemberType());
        System.out.println("Is Member: " + firstCustomer.isMember());
        System.out.println(firstCustomer.toString());

        System.out.println("\nTest for Discount Rates:\n");
        DiscountRate premium = new DiscountRate();
        System.out.println("The service discount rate of Premium is " + premium.getServiceDiscountRate("Premium"));
        System.out.println("The product discount rate of Premium is " + premium.getProductDiscountRate("Premium"));
        DiscountRate gold = new DiscountRate();
        System.out.println("The service discount rate of Gold is " + gold.getServiceDiscountRate("Gold"));
        System.out.println("The product discount rate of Gold is " + premium.getProductDiscountRate("Gold"));
        DiscountRate silver = new DiscountRate();
        System.out.println("The service discount rate of Silver is " + silver.getServiceDiscountRate("Silver"));
        System.out.println("The product discount rate of Silver is " + premium.getProductDiscountRate("Silver"));

        Visit firstVisitor = new Visit(" Guian Carlo ", new Date());
        firstVisitor.setProductExpense(1000);
        firstVisitor.setServiceExpense(700);
        System.out.println("Customer: " + firstVisitor.getName());
        System.out.println("Product Expense: " + firstVisitor.getProductExpense());
        System.out.println("Service Expense " + firstVisitor.getServiceExpense());
        System.out.println("Total Expense " + firstVisitor.getTotalExpense());
        System.out.println(firstVisitor.toString());

    }
}
