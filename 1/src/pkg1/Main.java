package pkg1;

public class Main {

    public static void main(String[] args) {
        //test program constructor and toString
        Shape shapeA = new Shape();
        System.out.println(shapeA.toString());

        //test program constructor and toString()
        Shape shapeB = new Shape("red", true);
        System.out.println(shapeB.toString());

        //test getter and setter
        shapeA.setColor("blue");
        System.out.println("The color is " + shapeA.getColor());
        System.out.println("The color is " + shapeB.getColor());
        System.out.println(shapeB.isFilled());
        System.out.println("\n");

        //test for constructor and toString()
        Circle circleA = new Circle();
        System.out.println(circleA.toString());
        Circle circleB = new Circle(2.0);
        Circle circleC = new Circle(2.0, "pink", true);
        System.out.println(circleC.toString());

        //test for getter and setter
        System.out.println("The circle has radius of "+ circleA.getRadius() + " and area of " + circleA.getArea());
        System.out.println("The circle has radius of "+ circleB.getRadius() + " and area of " + circleB.getArea());
        System.out.println("\n");

        //test program for rectangle
        Rectangle rectangleA = new Rectangle();
        System.out.println(rectangleA.toString());
        Rectangle rectangleB = new Rectangle(1.2f, 3.4f);
        System.out.println(rectangleB);  // toString()

        // Test setters and getters
        rectangleB.setLength(5.6f);
        rectangleB.setWidth(7.8f);
        System.out.println(rectangleB);  // toString()
        System.out.println("length is: " + rectangleB.getLength());
        System.out.println("width is: " + rectangleB.getWidth());

        // Test getArea() and getPerimeter()
        System.out.printf("area is: %.2f%n", rectangleB.getArea());
        System.out.printf("perimeter is: %.2f%n", rectangleB.getPerimeter());

        //test program for square constructor and toString()
        System.out.println("\n");
        Square squareA = new Square();
        System.out.println(squareA.toString());
        Square squareB = new Square(4.0);
        System.out.println(squareB.toString());
        Square squareC = new Square(3.0, "green", false);
        System.out.println(squareC.toString());

        //test for getter and setter
        System.out.println(squareA);
        System.out.println(squareA.getSide());
    }
}
